<?php
    require("./assets/php/valida-form.php");
?>

<!DOCTYPE html>
<!--[if IE 7 ]><html class="ie ie7 lte9 lte8 lte7" lang="en-US"><![endif]-->
<!--[if IE 8]><html class="ie ie8 lte9 lte8" lang="en-US">	<![endif]-->
<!--[if IE 9]><html class="ie ie9 lte9" lang="en-US"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="noIE" lang="en-US">
<!--<![endif]-->
	<head>
		<title>Fusão Injetora de Plástico</title>
        
        <link rel="shortcut icon" href="assets/images/fav-icon.png" />

		<!-- meta -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no"/>
		
		<!-- google fonts -->
		<link href='http://fonts.googleapis.com/css?family=Raleway:500,300' rel='stylesheet' type='text/css'>
		<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans'>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Droid+Serif:regular,bold"/>
		
		<!-- css -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/animate.css">
		<link rel="stylesheet" href="assets/css/style.css" media="screen"/>

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.js"></script>
			<script src="assets/js/respond.js"></script>
		<![endif]-->

		<!--[if IE 8]>
	    	<script src="assets/js/selectivizr.js"></script>
	    <![endif]-->
	</head>
	
	<body>

	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-79521665-1', 'auto');
		ga('send', 'pageview');

	</script>
        <div id="loader" class="loading">
            <img class="loading-logo" src="assets/images/fusao-logo-symbol.svg">
            <div class="loader"></div>
        </div>
		<header class="top-header">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-xs-5 col-sm-4 header-logo">
						<br>
						<a href="index.php">
                            <img class="logo" src="assets/images/fusao-logo-symbol.svg">
<!--							<h1 class="logo">Nevada <span class="logo-head">Plus</span></h1>-->
						</a>
					</div>

					<div class="col-md-8 col-md-offset-1 col-xs-7">
						<nav class="navbar navbar-default">
						  	<div class="container-fluid nav-bar">
						    <!-- Brand and toggle get grouped for better mobile display -->
							    <div class="navbar-header">
							      	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								        <span class="sr-only">Toggle navigation</span>
								        <span class="icon-bar"></span>
								        <span class="icon-bar"></span>
								        <span class="icon-bar"></span>
							      	</button>
							    </div>

						    	<!-- Collect the nav links, forms, and other content for toggling -->
							    <div class="collapse navbar-collapse navbar-def" id="bs-example-navbar-collapse-1">
							      
							      	<ul class="nav navbar-nav navbar-right">
							        	<li>
											<a href="#wrapper"><i class="fa fa-home"></i> Topo</a>
										</li>
										<li>
											<a href="#about"><i class="fa fa-info-circle"></i> Sobre</a>
										</li>
										<li>
											<a href="#clientes"><i class="fa fa-users"></i> Clientes</a>
										</li>
										<li>
											<a href="#contato"><i class="fa fa-envelope"></i> Contato</a>
										</li>
							      	</ul>
							    </div><!-- /navbar-collapse -->
						  	</div><!-- / .container-fluid -->
						</nav>
					</div>
				</div>
			</div>
		</header>
        
		<div id="wrapper">
			
			<div id="header" class="content-block">
				<section class="center">
					<div class="slogan">
                        <img class="img-responsive animated fadeInUp" style="margin: 0 auto;" src="assets/images/fusao-logo2.svg"/>
					</div>
					<div class="secondary-slogan">
						
					</div>
				</section>
			</div><!-- header -->

			<!-- About us -->
	        <div id="about" class="about-us">
	            <div class="container about-sec">
	                <header class="block-heading cleafix">
							<div class="title-page animTitle">
								<p class="main-header">Sobre</p>
                                <p class="separador"></p>
							    <p class="sub-header"> Conheça a nossa empresa</p>
							</div>
						</header>
	                <div class="divide50"></div>
	                <div class="row">
	                    <div class="col-md-4 text-center anim1">
	                        <div class="aboutus-item">
	                            <i class="aboutus-icon fa fa-industry"></i>
	                            <h4 class="aboutus-title">A Injetora</h4>
	                            <p class="aboutus-desc">A Fusão Injetora de Plásticos iniciou as atividades em Outubro de 2013 visando o mercado de injeção de peças em diversos tipos de polímeros termoplásticos.</p>
	                        </div>
	                    </div>
	                    <div class="col-md-4 text-center anim1">
	                        <div class="aboutus-item">
	                            <i class="aboutus-icon fa fa-thumbs-up"></i>
	                            <h4 class="aboutus-title">Qualidade</h4>
	                            <p class="aboutus-desc">Promovemos a melhoria continua de nossos processos e serviços para alcançar a real necessidade dos nossos clientes.</p>
	                        </div>
	                    </div>
	                    <div class="col-md-4 text-center anim1">
	                        <div class="aboutus-item">
	                            <i class="aboutus-icon fa fa-check"></i>
	                            <h4 class="aboutus-title">Objetivo</h4>
	                            <p class="aboutus-desc">Desenvolver produtos e serviços com o foco na qualidade e segurança, visando a continuidade do negócio.</p>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div> <!-- /About us -->

			<div class="content-block parallax" id="clientes">
				<div class="container services-sec">
					<div class="title-page animTitle">
						<p class="main-header">Clientes</p>
                        <p class="separador"></p>
					    <p class="sub-header">Empresas que atendemos</p>
					</div>
					<section class="block-body">
						<div class="row">
							<div class="col-md-4 anim2">
								<div class="service">
                                    <span class="helper"></span>
									<img src="assets/images/wap-logo.svg">
								</div>
							</div>
							<div class="col-md-4 anim2">
								<div class="service">
                                    <span class="helper"></span>
								    <img src="assets/images/mexbras-logo.png">
								</div>
							</div>
							<div class="col-md-4 anim2">
								<div class="service">
									<div class="bar">
										<i class="fa fa-thumbs-up special"></i>
									</div>	
									<h2 class="service-head">Sua empresa</h2>
									<p>Queremos a sua empresa aqui!.</p>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div><!-- #services -->

			<div class="content-block" id="contato">
				<div class="container">
					<div class="row">
							<div class="col-sm-8 blog-post anim1">
								<h2 class="footer-block">Entre em contato</h2>
								<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) . "#contato"; ?>" id="contactForm" method="post" name="contactform" class="" role="form">
									<div class="form-group">
								    	<input type="text" class="form-control form-control-white" id="nome" name="nome" placeholder="Nome" >
								  	</div>
								    <div class="form-group">
								    	<input type="email" class="form-control form-control-white" id="email" name="email" placeholder="E-mail" required>
								    </div>
								    <div class="form-group">
								    	<textarea class="form-control form-control-white" id="mensagem" name="mensagem" placeholder="Mensagem" required></textarea>
								    </div>
								    <div id="contactFormResponse">
                                      <?php if ($_SERVER["REQUEST_METHOD"] == "POST"): ?>
                                        <?php if (!$erro): ?>
                                          <div class="alert alert-success">
                                            Obrigado por sua mensagem!
                                            Entraremos em contato em breve.
                                              <?php // limpa o formulário.
                                                $nome = $email = $mensagem = "";
                                              ?>
                                          </div>
                                        <?php else: ?>
                                          <div class="alert alert-danger">
                                            Erros no formulário.
                                          </div>
                                        <?php endif; ?>
                                      <?php endif; ?>
                                    
                                    </div>
                                    <h6>* Preenchimento obrigatório.</h6>
								    <div class="form-group">
								    	<input id="cfsubmit" type="submit" class="text-center btn btn-o-white" value="Enviar">
								  	</div>
								</form>
							</div>
							<div class="col-sm-4 blog-post anim2">
								
								<h2 class="footer-block">Dados para Contato</h2>
								<ul>
									<li class="address-sub"><i class="fa fa-map-marker"></i>Endereço</li>
										<p>
											Rua Abraham Leiser Stier, 314 - Cidade Industrial - Curitiba-PR - CEP 81260-010. 
										</p>
									<li class="address-sub"><i class="fa fa-phone"></i>Telefone</li>
										<p>
											+55 (41) 9278-9257 <br>
                                            +55 (41) 8821-1170 <br>
                                            +55 (41) 9934-2022                                            
										</p>
									<li class="address-sub"><i class="fa fa-envelope-o"></i>E-mail</li>
										<p>
											<a href="mailto:contato@fusaoinjetora.com">contato@fusaoinjetora.com</a><br>
											<a href="http://fusaoinjetora.com/">fusaoinjetora.com</a>
										</p>
								</ul>
							</div>
						</div>
				</div>
			</div>
		</div><!--/#wrapper-->




		<script src="assets/js/jquery-2.1.3.min.js"></script>
		<script src="assets/js/bootstrap.js"></script>
		<script src="assets/js/jquery.actual.min.js"></script>
		<script src="assets/js/jquery.scrollTo.min.js"></script>
		<script src="assets/js/script.js"></script>
		<script src="assets/js/smoothscroll.js"></script>
		<script type="text/javascript">
		jQuery(document).ready(function($){
            
            document.getElementById("loader").style.display = "none";
            document.getElementById("wrapper").style.display = "block";
            
            $('.slogan img').removeClass('hidden');
            $('.slogan img').addClass('animated fadeInUp');

		  	$(window).scroll(function() {
		  		
				if ($(window).scrollTop() > 100 ){
		    
		 		$('.top-header').addClass('shows');
		    
		  		} else {
		    
		   	 	$('.top-header').removeClass('shows');
		    
		 		};   	
			});
            
            jQuery(".animTitle").viewportChecker({
                classToAdd: 'animated fadeInLeft',
                offset:10
            });
            
             jQuery(".anim1").viewportChecker({
                classToAdd: 'animated fadeInUp',
                offset:10
            });
            
            jQuery(".anim2").viewportChecker({
                classToAdd: 'animated fadeInDown',
                offset:10
            });

		  });

		jQuery('.scroll').on('click', function(e){		
				e.preventDefault()
		    
		  jQuery('html, body').animate({
		      scrollTop : jQuery(this.hash).offset().top
		    }, 1500);
		});


		</script>


	</body>
</html>
